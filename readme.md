# readme.md - README for Ssepan.Io v6.0

## About

Common library of i/o functions for C# applications; requires ssepan.utility
Ssepan.Io back-ported to Visual C# 2003 and .Net 1.1. Note: Going from later version requires giving up several features.
~ No Action type; used delegates
~ No generics; used overloads
~ No LINQ; used for and if/else
~ Different types for menu and toolbar; used ImageList for toolbar images
~ No 'default' keyword; used explicit nulls, enums, or values

### Purpose

To encapsulate common i/o functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

### History

6.0:
~initial release

Steve Sepan
ssepanus@yahoo.com
9/23/2022
